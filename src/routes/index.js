import {lazy} from "react";

const Products = lazy(() => import('../pages/produit'));
const ProductForm = lazy(() => import('../pages/produit/form'));
const Product = lazy(() => import('../pages/produit/show'));

export const routes = [
    {path : "/products", component: Products},
    {path : "/products/add", component: ProductForm},
    {path : "/products/:id", component: Product},
];