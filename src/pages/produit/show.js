import React, {Component} from "react";
import {getProduit} from "../../service/produit";
import Card from "reactstrap/es/Card";
import CardHeader from "reactstrap/es/CardHeader";
import CardBody from "reactstrap/es/CardBody";
import {Link} from "react-router-dom";

class Produit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            produit: {}
        }
    }

    async componentDidMount() {
        const produit = await getProduit(this.props.match.params.id);
        this.setState({produit})
    }

    render() {
        const {produit} = this.state;
        return <div className={"container mt-5"}>
            <Card>
                <CardHeader>{produit.name}</CardHeader>
                <CardBody>
                    <p>
                        {produit.description}
                    </p>
                    <Link to={"/products"}>
                        <button className={"btn-danger mt-3"}>
                            Retour
                        </button>
                    </Link>
                </CardBody>
            </Card>
        </div>
    }

}

export default Produit;