import React, {Component} from "react";
import {Card, CardHeader, CardBody, Table} from "reactstrap";
import {getProduits} from "../../service/produit";
import {Link} from "react-router-dom";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";


class Produits extends Component {

    constructor(props){
        super(props);
        this.state = {
            produits: []
        }
    }

    async componentDidMount() {
        const produits = await getProduits();
        await this.setState({produits})
    }

    addProduct = () => {
        this.props.history.push("/products/add");
    };

    showDetails = (id) => {
        this.props.history.push("/products/" + id);
    };

    render(){
        const {produits} = this.state;
        return <div className={'container mt-5'}>
            <Card>
                <CardHeader>
                    <Row>
                        <Col className={"col-6"}>
                            La liste des produits
                        </Col>
                        <Col className={"d-flex justify-content-end col-6"}>
                            <span className={"btn btn-success"} onClick={this.addProduct}>
                                Ajouter un produit
                            </span>
                        </Col>
                    </Row>
                </CardHeader>
                <CardBody>
                    <Table>
                        <tr>
                            <th>ID</th>
                            <th>Nom</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                        {
                            produits.map(produit => {
                                return <tr>
                                    <td>{produit.id}</td>
                                    <td>{produit.name}</td>
                                    <td>{produit.description}</td>
                                    <td>
                                        <button className={"btn btn-info"}
                                                onClick={() => this.showDetails(produit.id)}
                                        >
                                            Details
                                        </button>
                                    </td>
                                </tr>
                            })
                        }
                    </Table>
                    <Link to={"/"}>
                        <button className={"btn-danger mt-3"}>
                            Back Home
                        </button>
                    </Link>
                </CardBody>
            </Card>
        </div>
    }
}

export default Produits;