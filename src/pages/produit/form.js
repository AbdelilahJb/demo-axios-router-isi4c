import React, {Component} from "react";
import {Card, Form, CardHeader, CardBody, FormGroup, Label, Input} from "reactstrap";
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import {postProduit} from "../../service/produit";

class ProduitForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: ""
        };
    }

    onNameChange = (e) => {
        this.setState({name: e.target.value})
    };

    onDescriptionChange = (e) => {
        this.setState({description: e.target.value})
    };

    ajouterProduit = async () => {
        const {name, description} = this.state;
        const produit = {name, description};
        await postProduit(produit);
        this.props.history.push("/products");
    };

    render() {
        const {name, description} = this.state;
        return (
            <div className={"container mt-5"}>
                <Card>
                    <CardHeader>Ajouter un produit :</CardHeader>
                    <CardBody>
                        <Form>
                            <FormGroup>
                                <Label for="name">Nom</Label>
                                <Input
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="Nom du produit"
                                    value={name}
                                    onChange={this.onNameChange}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label for="description">Description</Label>
                                <Input
                                    value={description}
                                    type="textarea"
                                    name="description"
                                    id="description"
                                    placeholder="Description du produit"
                                    onChange={this.onDescriptionChange}
                                />
                            </FormGroup>
                            <Row className={"d-flex justify-content-end"}>
                                <Col className={"offset-11"}>
                                    <span
                                        className={'btn btn-outline-info'}
                                        onClick={this.ajouterProduit}
                                    >
                                        Ajouter
                                    </span>
                                </Col>
                            </Row>

                        </Form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}

export default ProduitForm;