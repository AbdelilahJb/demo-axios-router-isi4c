import React, {Fragment, Suspense} from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Produits from "./pages/produit";
import ProduitForm from "./pages/produit/form";
import Home from "./pages";
import Produit from "./pages/produit/show";
import {routes} from './routes';
import {isEmpty} from "lodash";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Switch>
                    <Route path={"/"} component={Home} exact={true}/>
                    <Suspense fallback={<div>Loading...</div>}>
                    {
                        !isEmpty(routes) ? routes.map(r => {
                            return <Route path={r.path} component={r.component} exact={true}/>
                        }) : <Fragment/>
                    }
                    </Suspense>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
