import API from "../../config/axios";
import {PRODUIT_ENDPOINT, PRODUIT_ENDPOINT_GET} from "../endpoints";

export const getProduits = () => API.get(PRODUIT_ENDPOINT)
    .then(response => response.data)
    .catch(response => response);

export const postProduit = (produit) => API.post(PRODUIT_ENDPOINT, produit)
    .then(response => response.data)
    .catch(response => response);

export const getProduit = (id) => API.get(PRODUIT_ENDPOINT_GET + "/" + id)
    .then(response => response.data)
    .catch(response => response);